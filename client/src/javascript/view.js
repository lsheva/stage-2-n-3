class View {
  element;

  createElement({
    tagName,
    className = "",
    attributes = {},
    onclick,
    text,
    value,
    disabled
  }) {
    const element = document.createElement(tagName);
    if (className) {
      element.classList.add(className);
    }
    Object.keys(attributes).forEach(key => {
      if (key !== "disabled" || attributes[key]) {
        element.setAttribute(key, attributes[key]);
      }
    });
    if (onclick) {
      element.addEventListener("click", onclick);
    }

    if (text !== undefined) {
      const textNode = document.createTextNode(text);
      element.appendChild(textNode);
    }

    if (value !== undefined) {
      element.value = value;
    }

    if (disabled !== undefined) {
      element.value = value;
    }

    return element;
  }
}

export default View;
