import View from "./view";
import App from "./app";

class HealthBar extends View {
  constructor(position) {
    super();
    this.health = 100;
    this.position = position;
    this.createHealthBar();
  }

  createHealthBar() {
    let className;
    if (this.position == "right") {
      className = "health-bar-right";
    } else {
      className = "health-bar-left";
    }

    this.healthElement = this.createElement({
      tagName: "div",
      className: "health-element"
    });

    this.element = this.createElement({
      tagName: "div",
      className: className
    });

    this.element.append(this.healthElement);

    this.updateHealth();
  }

  updateHealth(health) {
    if (health) {
      this.health = health;
    }
    this.healthElement.style.width = this.health + "%";
  }
}

export default HealthBar;
