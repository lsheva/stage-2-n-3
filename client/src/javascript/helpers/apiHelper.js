const API_URL = "http://localhost:3000/";

function callApi(endpoind, method, body) {
  const url = API_URL + endpoind;
  const options = {
    method,
    body: JSON.stringify(body),
    headers: { "Content-Type": "application/json" }
  };

  return fetch(url, options)
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error("Failed to load"))
    )
    .catch(error => {
      throw error;
    });
}

export { callApi };
