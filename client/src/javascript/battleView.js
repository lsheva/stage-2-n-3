import View from "./view";
import App from "./app";
import HealthBar from "./healthBarView";

class BattleView extends View {
  static playerPosition = 15;
  static attackShift = 10;
  static attackDuration = 250; // in milliseconds
  player1;
  player2;
  constructor(player1Details, player2Details) {
    super();
    this.createScene(player1Details, player2Details);
  }

  deleteChildren(element) {
    while (element.firstChild) {
      element.removeChild(element.firstChild);
    }
  }

  createScene(player1Details, player2Details) {
    // details - player parameters
    // status 0 default, 1 attack, -1 block
    // element - player dom element
    this.player1 = {
      details: player1Details,
      status: 0,
      health: player1Details.health,
      element: this.createElement({
        tagName: "img",
        className: "player1-element",
        attributes: {
          src: player1Details.source
        }
      })
    };
    this.player2 = {
      details: player2Details,
      status: 0,
      health: player2Details.health,
      element: this.createElement({
        tagName: "img",
        className: "player2-element",
        attributes: {
          src: player2Details.source
        }
      })
    };

    this.player1.healthBar = new HealthBar("left");
    this.player2.healthBar = new HealthBar("right");

    const footerElement = this.createElement({
      tagName: "div",
      className: "footer",
      text:
        "Use A/D letter keys for player 1 and left/right arrows for player 2"
    });

    this.deleteChildren(App.rootElement);
    App.rootElement.append(
      this.player1.element,
      this.player2.element,
      this.player1.healthBar.element,
      this.player2.healthBar.element,
      footerElement
    );
    this.updatePlayerPosition();

    document.onkeydown = e => {
      switch (e.keyCode) {
        case 65: // 'a' key
          if (this.player1.status == -1) break;
          this.player1.status = -1;
          this.updatePlayerPosition(1);
          this.checkDamage();
          setTimeout(() => {
            this.player1.status = 0;
            this.updatePlayerPosition(1);
          }, BattleView.attackDuration);
          break;
        case 68: // 'd' key
          if (this.player1.status == 1) break;
          this.player1.status = 1;
          this.updatePlayerPosition(1);
          this.checkDamage();
          setTimeout(() => {
            this.player1.status = 0;
            this.updatePlayerPosition(1);
          }, BattleView.attackDuration);
          break;
        case 39: // right arrow key
          if (this.player2.status == -1) break;
          this.player2.status = -1;
          this.updatePlayerPosition(2);
          this.checkDamage();
          setTimeout(() => {
            this.player2.status = 0;
            this.updatePlayerPosition(2);
          }, BattleView.attackDuration);
          break;
        case 37: // left arrow key
          if (this.player2.status == 1) break;
          this.player2.status = 1;
          this.updatePlayerPosition(2);
          this.checkDamage();
          setTimeout(() => {
            this.player2.status = 0;
            this.updatePlayerPosition(2);
          }, BattleView.attackDuration);
          break;
      }
    };
  }

  updatePlayerPosition(playerNumber) {
    if (playerNumber == 1 || !playerNumber) {
      const left =
        BattleView.playerPosition +
        this.player1.status * BattleView.attackShift;
      this.player1.element.style.left = left + "%";
    }

    if (playerNumber == 2 || !playerNumber) {
      const right =
        BattleView.playerPosition +
        this.player2.status * BattleView.attackShift;
      this.player2.element.style.right = right + "%";
    }
  }

  checkDamage() {
    const { player1, player2 } = this;

    if (player1.status == 1 && player2.status == 0) {
      player2.health -= this.calculateDamage(
        player1.details.attack,
        player2.details.defense
      );

      if (player2.health <= 0) {
        player2.health = 0;
      }

      player2.healthBar.updateHealth(
        (player2.health / player2.details.health) * 100
      );

      if (player2.health <= 0) {
        this.winsPopup(player1.details.name, "blue");
      }
    }
    if (player1.status == 0 && player2.status == 1) {
      player1.health -= this.calculateDamage(
        player2.details.attack,
        player1.details.defense
      );

      if (player1.health <= 0) {
        player1.health = 0;
      }

      player1.healthBar.updateHealth(
        (player1.health / player1.details.health) * 100
      );

      if (player1.health <= 0) {
        this.winsPopup(player2.details.name, "red");
      }
    }
  }

  calculateDamage(attack, defense) {
    const damage = attack * Math.random() - (defense * Math.random()) / 2;
    if (damage <= 0) return 0;
    return damage;
  }

  winsPopup(player, color) {
    const popup = this.createElement({
      tagName: "div",
      className: "wins-popup"
    });

    const text = this.createElement({
      tagName: "h1",
      text: `${player} wins`
    });

    text.style.color = color;

    const button = this.createElement({
      tagName: "button",
      className: "new-game-button",
      text: "New game",
      onclick: () => {
        this.deleteChildren(App.rootElement);
        new App();
      }
    });
    popup.append(text, button);
    App.rootElement.append(popup);
  }
}

export default BattleView;
