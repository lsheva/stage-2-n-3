import View from "./view";
import App from "./app";

class ModalView extends View {
  constructor(modal, handleClick, editableFieldsArray) {
    super();

    this.createModal(modal, handleClick, editableFieldsArray);
  }

  onSubmit(e, handleClick) {
    e.preventDefault();
    const data = this.fields.reduce((acc, cur) => {
      const input = cur.querySelector("input");
      acc[input.getAttribute("name")] = input.value;
      return acc;
    }, {});
    handleClick(data);
    this.element.remove();
  }

  createModal(modal, handleClick, editableFieldsArray) {
    this.fields = Object.keys(modal).map(key =>
      this.createField(key, modal[key], editableFieldsArray.includes(key))
    );

    this.element = this.createElement({
      tagName: "div",
      className: "modal-wrapper"
    });

    const form = this.createElement({
      tagName: "form",
      className: "modal"
    });

    const submit = this.createElement({
      tagName: "button",
      className: "submitButton",
      onclick: e => this.onSubmit(e, handleClick),
      text: "Save"
    });

    form.append(...this.fields, submit);
    this.element.append(form);
    App.rootElement.append(this.element);

    // Handle click outside modal
    this.element.onclick = e => {
      if (e.target == this.element && e.target !== form) {
        this.element.remove();
      }
    };
  }

  createField(name, value, editable = false) {
    const field = this.createElement({
      tagName: "div",
      className: "field"
    });

    const label = this.createElement({
      tagName: "label",
      text: name
    });

    const input = this.createElement({
      tagName: "input",
      attributes: {
        type: "text",
        name,
        value,
        disabled: !editable
      }
    });

    label.append(input);
    field.append(label);

    return field;
  }

  createSubmit(name, onSubmitHandler) {
    const submit = document.createElement("button");
    submit.innerHTML = name;
    submit.onclick = e => {
      e.preventDefault();
      const data = this.fields.reduce((acc, cur) => {
        const input = cur.querySelector("input");
        acc[input.getAttribute("name")] = input.value;
        return acc;
      }, {});
      onSubmitHandler(data);
    };
    return submit;
  }
}

export default ModalView;
