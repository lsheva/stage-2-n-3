import View from "./view";

class FighterView extends View {
  constructor(fighter, handleEditClick, fighterSelect) {
    super();

    this.createFighter(fighter, handleEditClick, fighterSelect);
  }

  createFighter(fighter, handleEditClick, fighterSelect) {
    const { name, source } = fighter;
    // fighter name
    const nameElement = this.createName(name);
    // fighter image
    const imageElement = this.createImage(source);
    // edit button
    const editElement = this.createElement({
      tagName: "div",
      className: "editButton",
      onclick: event => handleEditClick(event, fighter),
      text: "edit"
    });

    this.element = this.createElement({ tagName: "div", className: "fighter" });
    this.element.append(imageElement, nameElement, editElement);
    this.element.onclick = event => {
      const { player, selected } = fighterSelect(fighter);
      if (player !== false) {
        if (selected) {
          this.element.classList.add(player);
        } else {
          this.element.classList.remove(player);
        }
      }
    };
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: "span",
      className: "name"
    });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: "img",
      className: "fighter-image",
      attributes
    });

    return imgElement;
  }
}

export default FighterView;
