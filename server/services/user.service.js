const getName = user => {
  if (user.name) {
    return user.name;
  } else {
    return null;
  }
};

module.exports = { getName };
