const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.controller");

// отримання масиву всіх користувачів
router.get("/", userController.getAll);

// отримання одного користувача по ID
router.get("/:id", userController.getOne);

// створення користувача за даними з тіла запиту
router.post("/", userController.validate("user"), userController.create);

// оновлення користувача за даними з тіла запиту
router.put("/:id", userController.validate("user"), userController.update);

// видалення одного користувача по ID
router.delete("/:id", userController.delete);

module.exports = router;
